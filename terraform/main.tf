provider "google" {
  credentials = "${var.credentials_json}"
  project = "sandbox-gke-consul"
  region = "us-east4"
}

resource "google_container_cluster" "primary" {
  name = "sandbox-gke-consul"
  location = "us-east4"
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    machine_type = "n1-standard-4"

    metadata {
      disable-legacy-endpoints = "true"
    }

    labels = {
      owner = "nathan"
      ttl = "1w"
    }

    tags = ["nathan"]

    timeouts = {
      create = "10m"
      update = "10m"
    }
  }
}
